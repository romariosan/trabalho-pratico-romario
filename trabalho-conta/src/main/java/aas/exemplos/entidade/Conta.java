package aas.exemplos.entidade;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Wither;

@Data
@Entity
@Builder(toBuilder = true)
@Wither
@NoArgsConstructor
@AllArgsConstructor
public class Conta {
	@Id @GeneratedValue
	private Long id;
	private String nome;
	private String sobrenome;
	private String sexo;
	private String dtNascimento;
	private String telefone;
	
	public void setId(Long id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}

	
}
