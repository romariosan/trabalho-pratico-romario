package aas.exemplos.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import aas.exemplos.entidade.Conta;

public interface ContaRepositorio extends JpaRepository<Conta, Long>{
	
}
