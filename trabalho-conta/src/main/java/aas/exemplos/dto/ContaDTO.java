package aas.exemplos.dto;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_EMPTY;

import com.fasterxml.jackson.annotation.JsonInclude;

import aas.exemplos.entidade.Conta;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Wither;

@Data
@Builder(toBuilder = true)
@Wither
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(value = NON_EMPTY)
public class ContaDTO {
	
	private String nome;
	private String sobrenome;
	private String sexo;
	private String dtNascimento;
	private String telefone;
	
	
	public static Conta toConta(ContaDTO contaDTO) {
		return Conta.builder()
				.nome(contaDTO.getNome())
				.sobrenome(contaDTO.getSobrenome())
				.sexo(contaDTO.getSexo())
				.dtNascimento(contaDTO.getDtNascimento())
				.telefone(contaDTO.getTelefone())
				.build();
	}
	
	public static Conta fromConta(Conta conta) {
		return Conta.builder()
				.nome(conta.getNome())
				.sobrenome(conta.getSobrenome())
				.sexo(conta.getSexo())
				.dtNascimento(conta.getDtNascimento())
				.telefone(conta.getTelefone())
				.build();
	}


	
}
