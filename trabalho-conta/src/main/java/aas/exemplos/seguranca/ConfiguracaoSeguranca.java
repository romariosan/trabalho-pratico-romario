package aas.exemplos.seguranca;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
public class ConfiguracaoSeguranca extends WebSecurityConfigurerAdapter  {

	private static final String CONTA_URL = "/conta/**";
	private static final String ADMIN_ROLE = "ADMIN";

	private static final String USUARIO_UNICO = "admin ";

	@NonNull
    private  AcessoNegado acessoNegado;
	
	@NonNull
    private  PontoAutenticacao pontoAutenticacao;
	
	@NonNull
    private  TratamentoSucessoAutenticacao tratamentoSucesso;
	
    private final SimpleUrlAuthenticationFailureHandler tratamentoFalha = new SimpleUrlAuthenticationFailureHandler();
	
	@Override
	protected void configure(final AuthenticationManagerBuilder auth) throws Exception {
	    auth.inMemoryAuthentication()
	        .withUser(USUARIO_UNICO).password(encoder().encode(USUARIO_UNICO)).roles(ADMIN_ROLE);
	}
	
    @Override
    protected void configure(final HttpSecurity http) throws Exception {
        http.csrf().disable()
        	.authorizeRequests()
        	.and().exceptionHandling()
            	.accessDeniedHandler(acessoNegado)
            	.authenticationEntryPoint(pontoAutenticacao)
            .and().authorizeRequests()
                .antMatchers("/").hasAnyRole(ADMIN_ROLE)
            	.antMatchers("/contas/**").hasAnyRole(ADMIN_ROLE)
            	.antMatchers("/login**").permitAll()
            	.antMatchers("/logout**").hasAnyRole(ADMIN_ROLE)
            	.antMatchers(HttpMethod.GET, CONTA_URL).hasAnyRole(ADMIN_ROLE)
            	.antMatchers(HttpMethod.PUT, CONTA_URL).hasRole(ADMIN_ROLE)
            	.antMatchers(HttpMethod.POST, CONTA_URL).hasRole(ADMIN_ROLE)
            	.antMatchers(HttpMethod.DELETE, CONTA_URL).hasRole(ADMIN_ROLE)
        	.and().formLogin()
            	.successHandler(tratamentoSucesso)
            	.failureHandler(tratamentoFalha)
            .and().httpBasic()
            .and().logout();
    }
	
	@Bean
	public PasswordEncoder  encoder() {
	    return new BCryptPasswordEncoder();
	}
}
