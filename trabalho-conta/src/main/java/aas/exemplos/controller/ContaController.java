package aas.exemplos.controller;

import java.util.List;

import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import aas.exemplos.entidade.Conta;
import aas.exemplos.repository.ContaRepositorio;
import lombok.AllArgsConstructor;

@AllArgsConstructor
@RestController
public class ContaController {

	private final ContaRepositorio  contaRepositorio;
	
	@GetMapping("/conta")
	public List<Conta>  recuperaContas() {
		return contaRepositorio.findAll();
	}

	@PostMapping("/conta")
	public Conta cadastraConta(@RequestBody Conta conta) {
		return  contaRepositorio.save(conta); 
		
	}

	@PutMapping("/conta/{idCliente}")
	public Conta atualizaConta(@RequestBody Conta conta, @PathVariable Long idConta) {
		return this.contaRepositorio.findById(idConta)
				.map(contaEncontrada -> {
					
					Conta contaAlterada = contaRepositorio.save(contaEncontrada
							.withNome(conta.getNome())
							.withSexo(conta.getSexo()))
							.withSobrenome(conta.getSobrenome()).withDtNascimento(conta.getDtNascimento())
							.withTelefone(conta.getTelefone());
					
					return contaAlterada;
				})
				.orElse(cadastraConta(conta));
	}


	@DeleteMapping("/conta/{idConta}")
	public <T> ResponseEntity<T> excluiConta(@PathVariable Long idConta) {
		Conta existente = this.contaRepositorio.findById(idConta)
				.orElseThrow(() -> new ResourceNotFoundException(String.format("Não foi possível encontrar o conta com id %d", idConta)));
		this.contaRepositorio.deleteById(existente.getId());
		return ResponseEntity.noContent().build();
	}
}
